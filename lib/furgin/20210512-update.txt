// lands
Field of Ruin
Mana Confluence
Dark Depths

// colorless
Horn of Greed
Crucible of Worlds
Sword of Fire and Ice
Thorn of Amethyst

// Black
Wishclaw Talisman
Vampire Hexmage
Night's Whisper

// red
Greater Gargadon
Punishing Fire
Burn from Within
Orcish Lumberjack

// Green
Biogenic Ooze
Bloom Tender
Polukranos, World Eater
Goreclaw, Terror of Qal Sisma
Channel
Incubation Druid

// White
Anafenza, Kin-Tree Spirit
Kinjalli's Sunwing
Imposing Sovereign
Weathered Wayfarer
Giver of Runes
Suppression Field

// blue
Glen Elendra Archmage
Search for Azcanta
Pestermite

// gold
Neoform
