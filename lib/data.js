import csvParse from 'csv-parse/lib/sync.js';
import fs from 'fs/promises'
import path from 'path';
import _ from 'lodash';
import fetch from "node-fetch";
import BetterSqlite3 from 'better-sqlite3'
import {typeOrder} from "./types.js";
import pMemoize from 'p-memoize'

const debug = (msg) => process.env.DEBUG === "true" && console.log("DEBUG: " + msg);
const force = process.env.NODE_ENV === "test" || process.env.FORCE === "true";

const bestType = (types) => {
	types.sort((a, b) => {
		return typeOrder.indexOf(a) - typeOrder.indexOf(b)
	})
	return types[0];
}

const getData = pMemoize(() => download("https://mtgjson.com/api/v5/AllPrintings.sqlite",
	"AllPrintings.sqlite"))

export const connect = () => {
	return getData()
		.then(file => {
			debug('found file: ' + file);
			return BetterSqlite3(file);
		});
}

export function parse(line) {
	const arr = csvParse(line, {delimiter: "|"})[0]
	switch (arr.length) {
		case 1:
			return {
				name: arr[0],
			}
		case 2:
			return {
				set: arr[0].toUpperCase(),
				name: arr[1],
			}
		case 3:
			return {
				set: arr[0].toUpperCase(),
				collectorNumber: parseInt(arr[1]),
				name: arr[2],
			}
		case 4:
			return {
				set: arr[0].toUpperCase(),
				collectorNumber: parseInt(arr[1]),
				finish: arr[2].toLowerCase() === "foil" ? "Foil" : "Non-foil",
				name: arr[3],
			}
		default:
			throw new Error("unknown format: " + line)
	}
}

export function action(filename) {
	const arr = filename.match(/^(?:(\d*)-)?(?:(.*))?.txt$/)
	return {
		timestamp: arr[1] ? parseInt(arr[1]) : undefined,
		action: arr[2]
	}
}

export function read(file) {
	return fs.readFile(file)
		.then(buf => buf.toString())
		.then(str => str.split("\n"))
		.then(lines => Promise.all(lines
			.filter((line) => line.length > 0)
			.filter((line) => !line.trim().startsWith("//"))
			.map((line) => parse(line))))
}

function stat(dir) {
	return fs.stat(dir)
		.then((f) => f)
		.catch((e) => {
			return undefined;
		})
}

async function download(url, file) {
	let cacheFile = path.join('data', file);
	return fs.stat(cacheFile)
		.then((f) => cacheFile)
		.catch((e) => {
			console.log(`download ${url}...`)
			let dir = path.dirname(cacheFile);
			return stat(dir)
				.then((stat) => {
					return !!stat ? Promise.resolve() : fs.mkdir(dir);
				})
				.then(() =>
					fetch(url)
						.then(res => res.buffer())
						.then(buffer => fs.writeFile(cacheFile, buffer))
						.then(() => cacheFile)
				)
		})
}

export async function lookupCards(cards) {
	debug("connecting...")
	return connect()
		.then((db) => {
			// console.log("connected.")
			debug(`lookup ${cards.length} cards.`)
			return Promise
				.all(cards.map(c => lookup(db, c)))
				.finally(() => {
					debug("close...")
					db.close()
				})
		})
}

function colorCategory(dbCard) {

	const colors = {
		"U": "blue",
		"B": "black",
		"R": "red",
		"W": "white",
		"G": "green"
	}

	let colorCategory;
	let cardColorIdentity = dbCard["colorIdentity"] ? dbCard["colorIdentity"].split(",") : [];
	if (cardColorIdentity.length === 0) {
		colorCategory = "colorless"
	} else if (cardColorIdentity.length === 1) {
		colorCategory = colors[cardColorIdentity[0]]
	} else {
		colorCategory = "gold";
	}

	return colorCategory;
}

function cardDetails(card, found) {
	if (!found) {
		throw new Error("not found: " + card.name);
	}
	let name = card.name;
	if (card.faceName) name = card.faceName;
	if (card.layout === "split") name = card.name;
	return {
		name: name,
		cmc: parseInt(found.convertedManaCost),
		type: found.types,
		color: found.colors ? found.colors.split(",").join("") : "",
		set: found.setCode,
		collectorNumber: found.number,
		rarity: found.rarity,
		colorCategory: colorCategory(found),
		status: card.status,
		finish: card.finish ? card.finish : "Non-foil",
		maybeboard: false,
		imageURL: "", //image url
		imageBackURL: "", //image back url
		tags: "", //tags
		notes: "", //notes
		mtgoId: found.mtgoId ? parseInt(found.mtgoId) : undefined
	}
}

export async function lookup(db, card) {
	// debug("lookup: " + JSON.stringify(card))
	const {set, name} = card;
	return new Promise((res, rej) => {
		if (!!set) {
			const stmt = db.prepare('SELECT * From cards where (name=? or faceName=?) and setCode=?');
			let results = stmt.all([name, name, set]);
			res(cardDetails(card, results[0]))
		} else {
			const stmt = db.prepare('SELECT * From cards where (name=? or faceName=?)');
			let results = stmt.all([name, name]);
			res(cardDetails(card, results[0]))
		}

	})
}

export async function cube(name) {
	return jsonCache(() => loadCube(name), path.join("lib", `${name}.json`))
}

export async function jsonCache(fn, filename) {
	return stat(filename)
		.then((stat) => {
			if (force || !stat) {
				debug("not found: " + filename);
				return fn()
					.then((json) =>
						fs.writeFile(filename, JSON.stringify(json, null, 2))
							.then(() => json)
					)
			} else {
				debug("found: " + filename);
				return fs.readFile(filename)
					.then((buf) => JSON.parse(buf.toString()))
			}
		})
}

async function expand(cards) {
	return lookupCards(cards)
}

async function loadCube(name) {
	return fs.readdir(path.join("lib", name))
		.then((files) => {
			return Promise
				.all(files.map(f => fs.stat(path.join("lib", name, f))))
				.then(stats => files.filter((f, i) => !stats[i].isDirectory()))
		})
		.then((files) => {
			return Promise
				.all(files.map(f => read(path.join("lib", name, f))))
				.then((results) => {
					const data = files
						.reduce((acc, cur, index) => {
							acc.push({
								file: files[index],
								cards: results[index],
								count: results[index].length,
								...action(cur)
							})
							return acc;
						}, [])

					let cards = [];
					data.forEach(f => {
						let status = "Owned";

						switch (f.action) {
							case 'need':
								debug(`action: NEED (${f.action})`);
								status = "Not Owned";
								break;
							case 'printing':
							case 'print':
								debug(`action: PROXY (${f.action})`);
								status = "Proxied";
								break;
							default:
								debug(`action: ADD (${f.action})`);
								break;
						}

						f.cards.forEach((newCard) => {
							cards = cards.filter(c => c.name !== newCard.name);
							cards.push({
								name: newCard.name,
								status: status,
								finish: newCard.finish,
								set: newCard.set
							})
						})
					})
					return cards;
				})
				.then(cards => expand(cards))
				.then(cards => {
					return {
						name: name,
						cards
					}
				});
		});
}

