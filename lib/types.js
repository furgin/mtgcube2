export const typeOrder = [
	"Creature",
	"Planeswalker",
	"Instant",
	"Sorcery",
	"Enchantment",
	"Artifact",
	"Land",
	"Tribal"
]
