import {expect, test} from '@jest/globals'
import {connect, lookup, parse, action, cube} from './data.js'

test('parse with set', () => {
	expect(parse("set|Name"))
		.toEqual(expect.objectContaining({name: "Name", set: "SET"}))
})

test('parse with collector number', () => {
	expect(parse("set|1|Name"))
		.toEqual(expect.objectContaining({name: "Name", set: "SET", collectorNumber: 1}))
})

test('parse with collector number and foil', () => {
	expect(parse("set|1|foil|Name"))
		.toEqual(expect.objectContaining({name: "Name", set: "SET", finish: "Foil", collectorNumber: 1}))
})

test('parse just name', () => {
	expect(parse("Name"))
		.toEqual(expect.objectContaining({name: "Name"}))
})

test('lookup color identity', () => {
	return connect()
		.then(db => lookup(db, {set: "CMD", name: "Rakdos Signet"})
			.then((c) => {
				expect(c)
					.toEqual(
						expect.objectContaining({colorCategory: "gold"}))
			}))
})

test('lookup no set', () => {
	connect().then(db => lookup(db, {name: "Rakdos Signet"})
		.then((c) => {
			expect(c)
				.toEqual(
					expect.objectContaining({name: "Rakdos Signet"}))
		}));
})

test('parse action from filename', () => {
	expect(action('12345678-test.txt'))
		.toEqual(expect.objectContaining({timestamp: 12345678, action: 'test'}))
})

test('parse action no timezone', () => {
	expect(action('need.txt'))
		.toEqual(expect.objectContaining({timestamp: undefined, action: 'need'}))
})

test('load test cube', () => {
	return cube('test')
		.then(cube => {
			expect(cube.cards.length).toEqual(4);
			console.log(cube)

			expect(cube.cards[2].name).toEqual("Mother of Runes")
			expect(cube.cards[2].finish).toEqual("Foil")
		})
});