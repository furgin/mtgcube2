import {cube} from "./lib/data.js";
import * as CSV from 'csv-string';
import fs from 'fs';
import puppeteer from "puppeteer";

const isDebug = process.env.debug === "true";

async function login(page,email,password) {
	console.log("Login...")
	const [loginLink] = await page.$$("a.clickable.nav-link")
	await loginLink.click();
	await page.waitForSelector('.modal.fade.show', {visible: true})
	await page.type('#email', email)
	await page.type('#password', password)
	const [loginBtn] = await page.$$("button.btn.btn-outline-success.btn-block")
	isDebug && await page.screenshot({path: 'login.png'});
	await loginBtn.click();
	await page.waitForNavigation();
}

async function loadCube(page, id) {
	console.log("Load Cube: "+id)
	await page.goto('https://cubecobra.com/cube/list/'+id);
	isDebug && await page.screenshot({path: 'list.png'});
}

async function importCube(page,file) {
	console.log("Import File: "+file)

	const [importMenu] = await page.$x("//a[contains(., 'Import/Export')]");
	if (importMenu) {
		await importMenu.click();
	} else {
		throw new Error("import menu not found")
	}
	await page.waitForSelector('.dropdown-menu.show', {visible: true})
	isDebug && await page.screenshot({path: 'import-1.png'});

	const [importItem] = await page.$x("//button[contains(., 'Replace with CSV File Upload')]");
	if (importItem) {
		await importItem.click();
	} else {
		throw new Error("import menu item not found")
	}
	await page.waitForSelector('.modal.fade.show', {visible: true})

	const elementHandle = await page.$("input[type=file]");
	await elementHandle.uploadFile(file);
	isDebug && await page.screenshot({path: 'import-2.png'});

	const [uploadButton] = await page.$$(".modal.fade.show button[type='submit']");
	if (uploadButton) {
		await uploadButton.click();
	} else {
		throw new Error("upload button not found")
	}
	await page.waitForNavigation()
	await page.screenshot({path: 'import-3.png'});
}

async function upload({id,file,email,password}) {
	// const browser = await puppeteer.launch({ headless: false,devtools: true });
	const browser = await puppeteer.launch();
	const page = await browser.newPage();
	// page.on('console', (msg) => console.log('PAGE LOG:', msg.text()));

	await loadCube(page,id);
	await login(page,email,password);
	await importCube(page,file);

	isDebug && await page.screenshot({path: 'authenticated.png'});
	await browser.close();
}

upload({
	id: "60ab3845f37a345ffd6d76fe",
	file: "./furgin.csv",
	email: process.env.CC_USER,
	password: process.env.CC_PASSWORD
})
	.then(() => {
		console.log("done.")
	})

// cube(process.argv.slice(2)[0])
// 	.then((cube) => {
// 		cube.cards.sort((a, b) => {
// 			if (a.name < b.name) return -1;
// 			if (a.name > b.name) return 1
// 		})
//
// 		const list = cube.cards.map(card => {
// 			return [
// 				card.name,
// 				card.cmc,
// 				card.type,
// 				card.color,
// 				card.set,
// 				card.collectorNumber,
// 				card.rarity,
// 				card.colorCategory,
// 				card.status,
// 				card.finish,
// 				card.maybeboard,
// 				card.imageURL,
// 				card.imageBackURL,
// 				card.tags,
// 				card.nodes,
// 				card.mtgoId
// 			]
// 		});
//
// 		let csv = "";
//
// 		csv += CSV.stringify([
// 			"Name",
// 			"CMC",
// 			"Type",
// 			"Color",
// 			"Set",
// 			"Collector Number",
// 			"Rarity",
// 			"Color Category",
// 			"Status",
// 			"Finish",
// 			"Maybebord",
// 			"Image URL",
// 			"Image Back URL",
// 			"Tags",
// 			"Notes",
// 			"MTGO ID"
// 		]);
// 		csv += CSV.stringify(list)
//
// 		fs.writeFileSync(cube.name + ".csv", csv);
//
// 		if (process.env.UPLOAD === "true") {
// 			return upload();
// 		}
// 	})
//
